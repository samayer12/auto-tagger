# Auto-tagger

Auto tag to facilitate nightly deployments to production.

## Why do I care?

* Nightly releases help you understand potential errors from third-party sources (like security requirements).
* It reduces developer toil so your devs spend more time doing meaningful work instead of manually creating releases.
* You spend more time working on what matters and can mentor junior devs on project specifics instead of waiting for pipelines.

## Technical Details

This project demonstrates how to use `semantic-release` for nightly builds, trunk-based workflows, and merge-request based workflows.

Run a pipeline with the `CREATE_COMMIT` variable set to `1` to simulate development.
This creates a new commit to the project on `main` that meets the `patch` version criteria.
Finally, gitlab starts a `semantic-release` job and increments the `patch` version.

Be sure to set `GL_TOKEN` when testing.
Grant the token `api` access.
Expose `GL_TOKEN` as a variable in CI.

## Scheduled Jobs

* `Simulated development`: Runs the `create-commit.sh` script in CI, approximating someone working on this project.
* `Nightly build`: Runs `main` and tags if `semantic-release` requirements are met.

## Simulated Development Activity

* `create-commit.sh` will create a commit in the repo. This runs in CI using a project access token. `$CREATE_COMMIT` must be set.
* Programatically create a MR with the command:

```bash
git push --push-option=merge_request.create --push-option=merge_request.target=main --push-option=merge_request.merge_when_pipeline_succeeds --push-option=merge_request.remove_source_branch
```

* Set `$CREATE_MERGE_REQUEST` as a CI variable, then run the pipeline, to create a commit on a branch.

## Further Reading

* [Semantic Release](https://semantic-release.gitbook.io/semantic-release)
* [Angular Conventional Commits](https://www.conventionalcommits.org/)
* [Cron Job Syntax](crontab.guru)
* [Gitlab Scheduled Pipelines](https://docs.gitlab.com/ee/ci/pipelines/schedules.html)
* [Gitlab CI `!reference` tags](https://docs.gitlab.com/ee/ci/yaml/yaml_optimization.html#reference-tags)
* [Gitlab CI job control](https://docs.gitlab.com/ee/ci/jobs/job_control.html#common-if-clauses-for-rules)

## TODOs

* Learn more about rule customization
* Examine release rules (e.g. commit names) that make sense for teams
* Examine `autochangelog` generation
* ci-token expires on 1 Jul
* ci-token-maintainer expires on 1 Jul (write-repository)
