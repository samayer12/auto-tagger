#!/usr/bin/env bash

git push https://gitlab-ci-token:"${GL_TOKEN}"@gitlab.com/samayer12/auto-tagger --push-option=merge_request.create --push-option=merge_request.target=main --push-option=merge_request.merge_when_pipeline_succeeds --push-option=merge_request.remove_source_branch --set-upstream
